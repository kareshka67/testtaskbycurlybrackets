using Application.Exceptions;
using Interface;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using Xunit;
using Xunit.Abstractions;

namespace AttackSolverTest
{
	public class AttackSolverTest
	{
		private static Random randomizer = new Random();
		private readonly ITestOutputHelper output;

		public AttackSolverTest(ITestOutputHelper output)
		{
			this.output = output;
		}

		[Theory]
		[ClassData(typeof(AttackSolverTestValidData))]
		public void Valid_Points_Test(ChessmanType chessmanType, Size boardSize, Point chessmanCoords, Point[] obstacles, int expectedValue)
		{
			foreach (var attackCounter in FindImplementations())
			{
				output.WriteLine("Testing " + attackCounter.GetType().FullName);

				var res = attackCounter.CountUnderAttack(chessmanType, boardSize, chessmanCoords, obstacles);
				Assert.Equal(expectedValue, res);
			}
		}

		[Theory]
		[ClassData(typeof(AttackSolverTestInvalidData))]
		public void Invalid_Points_Test(ChessmanType chessmanType, Size boardSize, Point chessmanCoords, Point[] obstacles)
		{
			foreach (var attackCounter in FindImplementations())
			{
				output.WriteLine("Testing " + attackCounter.GetType().FullName);

				try
				{
					attackCounter.CountUnderAttack(chessmanType, boardSize, chessmanCoords, obstacles);
				}
				catch (Exception ex)
				{
					Assert.True(ex.GetType() == typeof(IncorrectCoordsException));
				}
			}
		}

		[Fact]
		public void Time_Test()
		{
			int size = 1_000;

			var sw = Stopwatch.StartNew();
			foreach (var attackCounter in FindImplementations())
			{
				for (int i = 0; i < 3; i++)
				{
					var chessmanCoords = GetRandomPoint();
					attackCounter.CountUnderAttack((ChessmanType)i, new Size(size, size), chessmanCoords, GetObstacles().Except(new Point[] { chessmanCoords }).ToArray());
				}
			}
			sw.Stop();

			Debug.Assert(sw.ElapsedMilliseconds < 100);

			IEnumerable<Point> GetObstacles()
			{
				for (int i = 0; i < size; i++)
				{
					yield return GetRandomPoint();
				}
			}

			Point GetRandomPoint() => new Point(randomizer.Next(1, size), randomizer.Next(1, size));
		}

		IList<IAttackCounter> FindImplementations()
		{
			var implementations = AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(mytype => mytype.GetInterfaces().Contains(typeof(IAttackCounter)))
				.Select(type => (IAttackCounter)Activator.CreateInstance(type)).ToList();

			if (implementations.Count == 0)
			{
				throw new Exception(
				   "No implementation of IAttackCounter was found, make sure you add a reference to your project to AttackSolverTest");
			}
			else
			{
				return implementations;
			}
		}
	}
}
