﻿using Interface;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace AttackSolverTest
{
	internal class AttackSolverTestValidData : IEnumerable<object[]>
	{
		public IEnumerator<object[]> GetEnumerator()
		{
			yield return TestDataForBishop();
			yield return TestDataForKnight();
			yield return TestDataForRook();
		}

		private object[] TestDataForRook()
		{
			var type = ChessmanType.Rook;
			var boardSize = new Size(3, 2);
			var chessmanCoords = new Point(1, 1);
			var obstacles = new[] { new Point(2, 2), new Point(3, 1) };
			int expectedValue = 2;

			return new object[] { type, boardSize, chessmanCoords, obstacles, expectedValue };
		}

		private object[] TestDataForBishop()
		{
			var type = ChessmanType.Bishop;
			var boardSize = new Size(4, 5);
			var chessmanCoords = new Point(2, 2);
			var obstacles = new[] { new Point(1, 1), new Point(1, 3) };
			int expectedValue = 3;

			return new object[] { type, boardSize, chessmanCoords, obstacles, expectedValue };
		}

		private object[] TestDataForKnight()
		{
			var type = ChessmanType.Knight;
			var boardSize = new Size(4, 5);
			var chessmanCoords = new Point(2, 2);
			var obstacles = new[] { new Point(1, 1), new Point(1, 3) };
			int expectedValue = 4;

			return new object[] { type, boardSize, chessmanCoords, obstacles, expectedValue };
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
