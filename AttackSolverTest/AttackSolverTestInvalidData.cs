﻿using Interface;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace AttackSolverTest
{
	internal class AttackSolverTestInvalidData : IEnumerable<object[]>
	{
		public IEnumerator<object[]> GetEnumerator()
		{
			yield return TestDataForBishop();
			yield return TestDataForKnight();
			yield return TestDataForRook();
		}

		private object[] TestDataForRook()
		{
			var type = ChessmanType.Rook;
			var boardSize = new Size(3, 2);
			var chessmanCoords = new Point(1, 1);
			var obstacles = new[] { new Point(2, 2), new Point(3, 4) };

			return new object[] { type, boardSize, chessmanCoords, obstacles };
		}

		private object[] TestDataForBishop()
		{
			var type = ChessmanType.Bishop;
			var boardSize = new Size(4, 5);
			var chessmanCoords = new Point(5, 2);
			var obstacles = new[] { new Point(1, 1), new Point(1, 3) };

			return new object[] { type, boardSize, chessmanCoords, obstacles };
		}

		private object[] TestDataForKnight()
		{
			var type = ChessmanType.Knight;
			var boardSize = new Size(4, 5);
			var chessmanCoords = new Point(2, 6);
			var obstacles = new[] { new Point(5, 1), new Point(1, 3) };

			return new object[] { type, boardSize, chessmanCoords, obstacles };
		}

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
