﻿using System.Drawing;

namespace Application.Entities
{
	public struct Board
	{
		public Board(Point boardStartPoint, Point boardEndPoint)
		{
			StartPoint = boardStartPoint;
			EndPoint = boardEndPoint;
			Obstacles = null;
		}

		public Board(Point boardStartPoint, Point boardEndPoint, Point[] obstacles)
		{
			StartPoint = boardStartPoint;
			EndPoint = boardEndPoint;
			Obstacles = obstacles;
		}

		/// <summary>
		/// Начало доски (клетка в нижнем левом углу доски).
		/// </summary>
		public Point StartPoint { get; }

		/// <summary>
		/// Конец доски (клетка в верхнем правом углу доски).
		/// </summary>
		public Point EndPoint { get; }

		/// <summary>
		/// Препятствия.
		/// </summary>
		public Point[] Obstacles { get; set; }
	}
}
