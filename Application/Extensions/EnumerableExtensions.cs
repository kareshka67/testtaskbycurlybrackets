﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Extensions
{
	public static class EnumerableExtension
	{
		public static Nullable<T> FirstOrNull<T>(this IEnumerable<T> source) where T : struct
		{
			return source.Any() ? source.First() : null as Nullable<T>;
		}

		public static Nullable<T> FirstOrNull<T>(this IEnumerable<T> source, Func<T, bool> predicate) where T : struct
		{
			return source.Any(predicate) ? source.First() : null as Nullable<T>;
		}

		public static Nullable<T> LastOrNull<T>(this IEnumerable<T> source) where T : struct
		{
			return source.Any() ? source.Last() : null as Nullable<T>;
		}

		public static Nullable<T> LastOrNull<T>(this IEnumerable<T> source, Func<T, bool> predicate) where T : struct
		{
			return source.Any(predicate) ? source.Last() : null as Nullable<T>;
		}
	}
}
