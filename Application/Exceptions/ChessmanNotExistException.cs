﻿using System;

namespace Application.Exceptions
{
	public class ChessmanCounterNotExistException : Exception
	{
		public ChessmanCounterNotExistException(string message)
		   : base(message)
		{ }
	}
}
