﻿using System;

namespace Application.Exceptions
{
	public class IncorrectCoordsException : Exception
    {
        public IncorrectCoordsException(string message)
            : base(message)
        { }
    }
}
