﻿using Application.Entities;
using Application.Extensions;
using Interface;
using System;
using System.Drawing;
using System.Linq;

namespace AttackSolver.ChessmansCounters
{
	public class RookCounter : IChessmanCounter
	{
		public int GetCountUnderAttack(Board board, Point chessmanCoords)
		{
			//Результат вычислений
			int totalCount = 0;

			//Найдём все препятствия для фигуры по вертикали и упорядочим их по возрастанию координаты У
			var obstaclesInTheVerticalLine = board.Obstacles
													.Where(point => point.X == chessmanCoords.X)
													.OrderBy(p => p.Y)
													.ToList();
			//А все препятствия по горизонтали упорядочим по возрастанию координаты Х
			var obstaclesInTheHorizontalLine = board.Obstacles
													.Where(point => point.Y == chessmanCoords.Y)
													.OrderBy(p => p.X)
													.ToList();
			
			var nearestObstacle = obstaclesInTheVerticalLine.FirstOrNull(obstacle => obstacle.Y > chessmanCoords.Y);    //Найдём ближайшее препятствие НАД фигурой:
			totalCount += FindDistance( nearestObstacle?.Y,
										board.StartPoint.Y, 
										chessmanCoords.Y, 
										board.EndPoint.Y);	

			nearestObstacle = obstaclesInTheVerticalLine.LastOrNull(obstacle => obstacle.Y < chessmanCoords.Y);			//Найдём ближайшее препятствие ПОД фигурой:
			totalCount += FindDistance( nearestObstacle?.Y,
										board.StartPoint.Y, 
										chessmanCoords.Y);		
			
			nearestObstacle = obstaclesInTheHorizontalLine.FirstOrNull(obstacle => obstacle.X > chessmanCoords.X);		//Найдём ближайшее препятствие СПРАВА от фигуры:
			totalCount += FindDistance( nearestObstacle?.X, 
										board.StartPoint.X, 
										chessmanCoords.X, 
										board.EndPoint.X);	
			
			nearestObstacle = obstaclesInTheHorizontalLine.LastOrNull(obstacle => obstacle.X < chessmanCoords.X);       //Найдём ближайшее препятствие СЛЕВА от фигуры:
			totalCount += FindDistance( nearestObstacle?.X, 
										board.StartPoint.X, 
										chessmanCoords.X);      

			return totalCount;
		}

		/// <summary>
		/// Вычислить и прибавим к результату количество клеток до препятствия, а если его нет - до края доски.
		/// </summary>
		/// <param name="nearestObstacleCoord">Координата препятствия на прямой</param>
		/// <param name="boardStartPointCoord">Координата точки начала отсчета</param>
		/// <param name="chessmanCoord">Координата фигуры</param>
		/// <param name="boardEndPointCoord">Координата дальней точки доски</param>
		/// <returns></returns>
		private int FindDistance(int? nearestObstacleCoord, int boardStartPointCoord, int chessmanCoord, int? boardEndPointCoord = null)
		{
			if (nearestObstacleCoord.HasValue)
			{
				return Math.Abs(nearestObstacleCoord.Value - chessmanCoord) - 1;
			}

			return boardEndPointCoord.HasValue
									? boardEndPointCoord.Value - chessmanCoord
									: chessmanCoord - boardStartPointCoord;
		}
	}
}
