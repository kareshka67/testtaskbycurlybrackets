﻿using Application.Entities;
using Application.Extensions;
using Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AttackSolver.ChessmansCounters
{
	public class BishopCounter : IChessmanCounter
	{
		public int GetCountUnderAttack(Board board, Point ChessmanCoords)
		{
			int totalCount = 0;

			//Ищем препятствия по диагоналям от фигуры и сортируем их по отдалённости от фигуры 
			//(т.к. вектор движения задаётся одинаковым смещением обеих координат, то сортировать можем как по разности X, так и по разности Y отдельно, а не по их совокупности)
			var obstaclesInTheWay = board.Obstacles.Where(o => Math.Abs(o.X - ChessmanCoords.X) == Math.Abs(o.Y - ChessmanCoords.Y))
										 .OrderBy(o => Math.Abs(o.X - ChessmanCoords.X))
										 .ToList();

			totalCount += GetLeftDownCount(board, ChessmanCoords, obstaclesInTheWay);

			totalCount += GetRightUpCount(board, ChessmanCoords, obstaclesInTheWay);

			totalCount += GetRightDownCount(board, ChessmanCoords, obstaclesInTheWay);

			totalCount += GetLeftUpCount(board, ChessmanCoords, obstaclesInTheWay);

			return totalCount;
		}

		private int GetLeftDownCount(Board board, Point ChessmanCoords, IEnumerable<Point> obstaclesList)
		{
			var leftDownObstacle = obstaclesList.Where(o => o.X < ChessmanCoords.X)
												.Where(o => o.Y < ChessmanCoords.Y)             //Ищем ближайшее препятствие по направлению ВНИЗ и ВЛЕВО
												.FirstOrNull();

			return leftDownObstacle.HasValue                                                        //Если препятствие найдено
								? GetCountToObstacle(leftDownObstacle.Value, ChessmanCoords)        //то ищем расстояние до него (без разницы по какой координате).
								: GetCountToNearestSideOfBoard();									//Иначе - ищем расстояние до края доски. 

			int GetCountToNearestSideOfBoard() => ChessmanCoords.X < ChessmanCoords.Y				//Если до левого края расстояние меньше, чем до нижнего,
													? ChessmanCoords.X - board.StartPoint.X         //то прибавляем к результату количество клеток до левого края
													: ChessmanCoords.Y - board.StartPoint.Y;        //иначе - прибавляем количество клеток до нижнего края	
		}

		private int GetRightUpCount(Board board, Point ChessmanCoords, IEnumerable<Point> obstaclesList)
		{
			var rightUpObstacle = obstaclesList.Where(o => o.X > ChessmanCoords.X)
											   .Where(o => o.Y > ChessmanCoords.Y)                          //Ищем ближайшее препятствие по направлению ВВЕРХ и ВПРАВО
											   .FirstOrNull();

			return rightUpObstacle.HasValue                                                                     //Если препятствие найдено
								? GetCountToObstacle(rightUpObstacle.Value, ChessmanCoords)                     //то ищем расстояние до него (без разницы по какой координате).
								: GetCountToNearestSideOfBoard();												//Иначе - ищем расстояние до края доски. 

			int GetCountToNearestSideOfBoard() => ((board.EndPoint.X - ChessmanCoords.X) < (board.EndPoint.Y - ChessmanCoords.Y)	//Если до правого края расстояние меньше, чем до верхнего,
													? board.EndPoint.X - ChessmanCoords.X											//то прибавляем к результату количество клеток до правого края,
													: board.EndPoint.Y - ChessmanCoords.Y);											//иначе - прибавляем количество клеток до верхнего края.
		}

		private int GetRightDownCount(Board board, Point ChessmanCoords, IEnumerable<Point> obstaclesList)
		{
			var rightDownObstacle = obstaclesList.Where(o => o.X > ChessmanCoords.X)
												 .Where(o => o.Y < ChessmanCoords.Y)            //Ищем ближайшее препятствие по направлению ВНИЗ и ВПРАВО 
												 .FirstOrNull();

			return rightDownObstacle.HasValue                                                       //Если препятствие найдено 
								? GetCountToObstacle(rightDownObstacle.Value, ChessmanCoords)       //то ищем расстояние до него (без разницы по какой координате).
								: GetCountToNearestSideOfBoard();                                   //Иначе - ищем расстояние до края доски.

			int GetCountToNearestSideOfBoard() =>(ChessmanCoords.Y > (board.EndPoint.X - ChessmanCoords.X)          //Если до правого края расстояние меньше, чем до нижнего,
													? board.EndPoint.X - ChessmanCoords.X							//то прибавляем к результату количество клеток до верхнего края,
													: ChessmanCoords.Y - board.StartPoint.Y);                       //иначе - прибавляем количество клеток до левого края.
		}

		private int GetLeftUpCount(Board board, Point ChessmanCoords, IEnumerable<Point> obstaclesList)
		{
			var leftUpObstacle = obstaclesList.Where(o => o.X < ChessmanCoords.X)
											  .Where(o => o.Y > ChessmanCoords.Y)               //Ищем ближайшее препятствие по направлению ВВЕРХ и ВЛЕВО
											  .FirstOrNull();

			return leftUpObstacle.HasValue                                                          //Если препятствие найдено 
								? GetCountToObstacle(leftUpObstacle.Value, ChessmanCoords)          //то ищем расстояние до него (без разницы по какой координате).
								: GetCountToNearestSideOfBoard();

			int GetCountToNearestSideOfBoard() => (ChessmanCoords.X > (board.EndPoint.Y - ChessmanCoords.Y)          //Иначе - ищем расстояние до края доски. Если до верхнего края расстояние меньше, чем до левого,                            
													? board.EndPoint.Y - ChessmanCoords.Y                            //то прибавляем к результату количество клеток до верхнего края,
													: ChessmanCoords.X - board.StartPoint.X);                        //иначе - прибавляем количество клеток до левого края.
		}

		private int GetCountToObstacle(Point obstacle, Point chessman) => Math.Abs(obstacle.X - chessman.X) - 1;	//Отнимаем единицу, т.к. нам не нужно учитывать клетку с самим препятствием, 
																													//необходимо найти количество клеток между фигурой и препятствием.
	}
}
