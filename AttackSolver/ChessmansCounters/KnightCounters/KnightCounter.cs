﻿using Application.Entities;
using AttackSolver.Validators;
using Interface;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AttackSolver.ChessmansCounters
{
	public class KnightCounter : IChessmanCounter
	{
		/// <summary>
		/// Список векторов всех возможных перемещений фигуры из текущей точки.
		/// </summary>
		private static IEnumerable<Point> _potentialSteps => new List<Point>
		{
			new Point(-2, -1), new Point(-2, 1),
			new Point(-1, -2), new Point(-1, 2),
			new Point(1, -2), new Point(1, 2),
			new Point(2, -1), new Point(2, 1)
		};

		public int GetCountUnderAttack(Board board, Point ChessmanCoords)
		{
			int totalCount = 0;

			//Валидатор координат точки
			var coordsValidator = PointCoordsValidator.Instance;

			foreach (var potentialStep in _potentialSteps)
			{
				//Найдём координаты клетки, в которую попадёт фигуры при попытке выполнить ход в заданном направлении
				var ghostPoint = new Point(ChessmanCoords.X + potentialStep.X, ChessmanCoords.Y + potentialStep.Y);

				if (!board.Obstacles.Contains(ghostPoint)												//Проверим не находится ли в этой точке препятствие,
					&& coordsValidator.IsPointOnBoard(board.StartPoint, board.EndPoint, ghostPoint))	//и находится ли эта точка в пределах доски
				{
					totalCount++;
				}
			}

			return totalCount;
		}
	}
}
