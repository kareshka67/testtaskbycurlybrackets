﻿using Application.Entities;
using Application.Exceptions;
using AttackSolver.ChessmansCounters;
using AttackSolver.Validators;
using Interface;
using System.Collections.Generic;
using System.Drawing;

namespace AttackSolver.Counters
{
	public class ExtendedAttackCounter : IAttackCounter
    {
		/// <summary>
		/// Словарь, определяющий для фигуры каждого типа соответствующий алгоритм расчета
		/// </summary>
		private Dictionary<ChessmanType, IChessmanCounter> _typesToCountersDict => new Dictionary<ChessmanType, IChessmanCounter>
			{
				{ ChessmanType.Bishop, new BishopCounter() },
				{ ChessmanType.Knight, new KnightCounter() },
				{ ChessmanType.Rook, new RookCounter() },
			};

		public int CountUnderAttack(ChessmanType cmType, Size boardSize, Point chessmanCoords, Point[] obstacles)
		{
			//По заданию: начало отсчёта находится в точке (1;1)
			var boardStartPoint = new Point(1, 1);
			var board = GetNormalizedBoardWithObstacles(boardSize, boardStartPoint, obstacles);

			//Проверим координаты препятсвий и фигуры на валидность
			if (!PointCoordsValidator.Instance.GetPointsValidationResult(board, chessmanCoords))
			{
				throw new IncorrectCoordsException("Введены некорректные координаты точек");
			}

			//Получим результат для выбранной фигуры
			if (!_typesToCountersDict.TryGetValue(cmType, out var chessmanCounter))
			{
				throw new ChessmanCounterNotExistException("Алгоритм для выбранной фигуры не найден");
			}

			return chessmanCounter.GetCountUnderAttack(board, chessmanCoords);
		}

		/// <summary>
		/// Получить доску с препятствиями, левый нижний угол которой является началом координат
		/// </summary>
		/// <param name="boardSize">Размер доски</param>
		/// <param name="boardStartPoint">Начало координат</param>
		/// <param name="obstacles">Препятствия</param>
		/// <returns></returns>
		private Board GetNormalizedBoardWithObstacles(Size boardSize, Point boardStartPoint, Point[] obstacles)
		{
			var boardEndPointX = boardSize.Width - 1 + boardStartPoint.X;
			var boardEndPointY = boardSize.Height - 1 + boardStartPoint.Y;
			
			var boardEndPoint = new Point(boardEndPointX, boardEndPointY);

			return new Board(boardStartPoint, boardEndPoint, obstacles);
		}
	}
}
