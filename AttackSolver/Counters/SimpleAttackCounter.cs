﻿using Application.Exceptions;
using Interface;
using System.Drawing;

namespace AttackSolver.Counters
{
    public class SimpleAttackCounter : IAttackCounter
    {
        public int CountUnderAttack(ChessmanType cmType, Size boardSize, Point startCoords, Point[] obstacles)
        {
            switch (cmType)
            {
                case ChessmanType.Rook:
                    return 2;
                case ChessmanType.Bishop:
                    return 3;
                case ChessmanType.Knight:
                    return 4;
				default:
					throw new ChessmanCounterNotExistException("Выбранная фигура не существует");
			}
        }
    }
}
