﻿using Application.Entities;
using AttackSolver.Interface;
using System;
using System.Drawing;

namespace AttackSolver.Validators
{
	public sealed class PointCoordsValidator : IPointCoordsValidator
	{
		private static readonly Lazy<PointCoordsValidator> _instance = new Lazy<PointCoordsValidator>(() => new PointCoordsValidator());
		private PointCoordsValidator()
		{ }

		public static PointCoordsValidator Instance { get => _instance.Value; }

		public bool IsPointOnBoard(Point boardStartPoint, Point boardEndPoint, Point point) => boardStartPoint.X <= point.X && point.X <= boardEndPoint.X  //клетка внутри доски по оси Х
																				&& boardStartPoint.Y <= point.Y && point.Y <= boardEndPoint.Y; //клетка внутри доски по оси У

		public bool GetPointsValidationResult(Board board, Point chessmanCoords )
		{
			if (!IsPointOnBoard(board.StartPoint, board.EndPoint,  chessmanCoords))
			{
				return false;
			}

			for (int i = 0; i < board.Obstacles.Length; i++)
			{
				if (!IsPointOnBoard(board.StartPoint, board.EndPoint, board.Obstacles[i]) || chessmanCoords == board.Obstacles[i])
				{
					return false;
				}
			}

			return true;
		}
	}
}
