﻿using System.Drawing;
using Application.Entities;

namespace AttackSolver.Interface
{
	public interface IPointCoordsValidator
	{
		/// <summary>
		/// Валидация набора объектов, расположенных на клетках поля.
		/// </summary>
		/// <param name="board">Доска</param>
		/// <param name="chessmanCoords">Координаты фигуры</param>
		/// <returns></returns>
		bool GetPointsValidationResult(Board board, Point chessmanCoords);

		/// <summary>
		/// Проверка на расположение клетки внутри заданного поля.
		/// </summary>
		/// <param name="boardStartPoint">Клетка, являющаяся началом координат</param>
		/// <param name="boardEndPoint">Самая дальняя от начала координат клетка доски</param>
		/// <param name="point">Проверяемая клетка</param>
		/// <returns></returns>
		bool IsPointOnBoard(Point boardStartPoint, Point boardEndPoint, Point point);
	}
}