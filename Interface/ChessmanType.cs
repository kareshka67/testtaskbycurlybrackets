﻿namespace Interface
{
	public enum ChessmanType
	{
		/// <summary>
		/// Ладья.
		/// </summary>
		Rook,

		/// <summary>
		/// Слон.
		/// </summary>
		Bishop,

		/// <summary>
		/// Конь.
		/// </summary>
		Knight
	}
}
