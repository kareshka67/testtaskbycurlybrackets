﻿using System.Drawing;

namespace Interface
{
    public interface IAttackCounter
    {
		/// <summary>
		/// Returns number of points under attack.
		/// </summary>
		/// <param name="cmType">Тип фигуры</param>
		/// <param name="boardSize">Размер доски</param>
		/// <param name="startCoords">координата фигуры</param>
		/// <param name="obstacles">координаты препятствий</param>
		/// <returns></returns>
		int CountUnderAttack(ChessmanType cmType, Size boardSize, Point startCoords, Point[] obstacles);
    }
}
