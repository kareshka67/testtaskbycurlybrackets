﻿using System.Drawing;
using Application.Entities;

namespace Interface
{
	public interface IChessmanCounter
	{
		/// <summary>
		/// Получить значение всех доступных для атаки клеток.
		/// </summary>
		/// <param name="board"></param>
		/// <param name="chessmanCoords"></param>
		/// <returns></returns>
		int GetCountUnderAttack(Board board, Point chessmanCoords);
	}
}
